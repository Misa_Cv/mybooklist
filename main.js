//Book class: Represents a book

class Book {
	constructor(title,author,isbn){
		this.title=title;
		this.author=author;
		this.isbn=isbn;
	}
}

//UI class : Handle UI task

class UI{
	static displayBooks(){
		
		const books = Store.getBooks();
		books.forEach((book) => UI.addBookToList(book));

		
		}
	
	static addBookToList(book){
			const list = document.querySelector("#book-list");


			const row = document.createElement("tr");

			row.innerHTML = `
			<td>${book.title}</td>
			<td>${book.author}</td>
			<td>${book.isbn}</td>
			<td><a href="#" class="btn btn-danger btn-sm delete">X</a></td>
			`;

			list.appendChild(row);
	}

	static deleteBook(el){

		if(el.classList.contains('delete')){
			el.parentElement.parentElement.remove();
		}
	}




	static ShowAlert(message,className){
		const div = document.createElement('div');
		div.className=`alert alert-${className}`;
		div.appendChild(document.createTextNode(message));
		const container = document.querySelector('.container');
		const form = document.querySelector('#book-form');
		container.insertBefore(div,form);

		//vanish in 3 second
		setTimeout(()=>document.querySelector('.alert').remove(),
			3000);
	}

	static ClearField(){
		document.querySelector('#title').value='';
		document.querySelector('#author').value='';
		document.querySelector('#isbn').value='';
	}

}

//store Class : handles storage
class Store {
	static getBooks() {
		let books;
		if(localStorage.getItem('books') === null){
			books=[];

		}else{
			books= JSON.parse(localStorage.getItem('books'));
		}
		return books;
	}
	static addBooks(book) {
		const books = Store.getBooks();

		books.push(book);

		localStorage.setItem('books',JSON.stringify(books));
	}
	static	removeBooks(isbn) {
		const books = Store.getBooks();

		books.forEach((book,index)=>{
			if(book.isbn === isbn){
				books.splice(index,1)
			}
		});

		localStorage.setItem('books',JSON.stringify(books));
	}
}


//Event :Display Books

document.addEventListener('DOMContentLoaded', UI.displayBooks);

//Event Add Book

document.querySelector("#book-form").addEventListener('submit',(e)=>{
//get form values
 const title = document.querySelector("#title").value;
 const author= document.querySelector("#author").value;
 const isbn = document.querySelector("#isbn").value;

 //validate
 if(title === '' || author === '' || isbn === ''){
 UI.ShowAlert("Molimo vas popunite sva polja!! ","danger")
 }else{
 	//Institiate book
const book = new Book(title,author,isbn);
//add book to list
UI.addBookToList(book);

//add book to storage
Store.addBooks(book);

//Clear Fields
UI.ClearField();
UI.ShowAlert("Uspesno ste dodali knjigu ","success")
 }



});

//Event : remove book

document.querySelector('#book-list').addEventListener('click',(e)=>{
	//Remove book from UI
	UI.deleteBook(e.target);
	
	//Remove book from storage
	Store.removeBooks(e.target.parentElement.previousElementSibling.textContent);
//show message
	UI.ShowAlert("Uspesno ste obrisali knjigu","info");
});

